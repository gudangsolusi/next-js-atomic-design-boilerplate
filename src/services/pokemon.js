import api from "@/services/api";
export const getAllPokemon = () => {
  return api.get("pokemon?limit=25");
};

export const getPokemonByName = (name) => {
  return api.get(`pokemon/${name}`);
};
