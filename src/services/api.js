import axios from "axios";
import Router from "next/router";
import querystring from "qs";

const BASE_URL = process.env.NEXT_PUBLIC_BASE_URL;
const TIMEOUT = 70000;

const refreshTokenEndpoint = "user/token/refresh";
const loginPath = "/login",
  refreshTokenKey = "refreshToken",
  accessTokenKey = "accessToken";
const isServer = typeof window === "undefined";
const api = axios.create({
  baseURL: BASE_URL,
  timeout: TIMEOUT,
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
  },
  paramsSerializer: (params) => querystring.stringify(params),
});

const APIResponseValidation = async (response, promise, needAuth = true) => {
  // prevent refresh token checking when func executed in server or needAuth parameter is false
  if (isServer) return promise;

  const accessToken = await localStorage.getItem(accessTokenKey);
  const refreshToken = await localStorage.getItem(refreshTokenKey);

  // check if access token is not exist / not login
  if (response?.status === 401) {
    if (!needAuth) return promise;

    if (!accessToken) {
      const homeUrl = window?.location?.origin;
      Router?.replace(
        `${homeUrl}${loginPath}?redirect=${window?.location?.pathname || ""}${
          window?.location?.search || ""
        }`,
      );
      return promise;
    }

    try {
      const refreshTokenResponse = await APIInstance.post(
        refreshTokenEndpoint,
        null,
        {
          refreshToken,
        },
      );

      localStorage.setItem(
        refreshTokenKey,
        refreshTokenResponse.data.refresh_token,
      );
      localStorage.setItem(
        accessTokenKey,
        refreshTokenResponse.data.access_token,
      );

      return axios
        .request({
          ...response.config,
          headers: {
            ...response.config.headers,
            needAuthorization: `Bearer ${refreshTokenResponse.data.access_token}`,
          },
        })
        .then((response) => {
          APIResponseValidation(response);
          return Promise.resolve(response);
        })
        .catch((err) => {
          APIResponseValidation(err.response);
          return Promise.reject(err);
        });
    } catch (err) {
      // logout the user
      console.log(err);
      logout();
    }
  }
  // show toast error message if API not success
  else if (response?.status && response?.status !== 200) {
    // check refresh token
    if (showErrorPage) {
      //   APIResponseErrorValidation({ response });
      return promise;
    }
  }
  return promise;
};

const APIInstance = {
  /**
   * @param {Sring} url '/path/to/endpoint'
   * @param {Object} json
   * @param {Object} form
   */
  put: (url, form = {}, json = {}, customConfig = {}, needAuth = true) => {
    api.defaults.headers.common["Content-Type"] = json
      ? "application/json"
      : "application/x-www-form-urlencoded";
    const data = querystring.stringify(form) || json;
    return api
      .put(url, data, {
        params: querystring.stringify(form),
        ...customConfig,
      })
      .then((response) => {
        return APIResponseValidation(
          response,
          Promise.resolve(response),
          needAuth,
        );
      })
      .catch((err) => {
        return APIResponseValidation(
          err.response,
          Promise.reject(err),
          needAuth,
        );
      });
  },

  /**
   * @param {Sring} url '/path/to/endpoint'
   * @param {Object} param query params
   */
  get: (url, customConfig = {}, needAuth = true) => {
    return api
      .get(url, {
        ...customConfig,
      })
      .then((response) => {
        return APIResponseValidation(
          response,
          Promise.resolve(response),
          needAuth,
        );
      })
      .catch((err) => {
        return APIResponseValidation(
          err.response,
          Promise.reject(err),
          needAuth,
        );
      });
  },

  /**
   * @param {Sring} url '/path/to/endpoint'
   * @param {Object} json
   * @param {Object} form
   * @param {Object} reqConfig  custom config for request
   */
  post: (url, form = null, json = {}, reqConfig = {}) => {
    api.defaults.headers["Content-Type"] = form
      ? "application/x-www-form-urlencoded"
      : "application/json";

    const data = querystring.stringify(form) || json;
    return api
      .post(url, data, {
        params: querystring.stringify(form),
        ...reqConfig,
      })
      .then((response) => {
        return APIResponseValidation(
          response,
          Promise.resolve(response),
          needAuth,
        );
      })
      .catch((err) => {
        return APIResponseValidation(
          err.response,
          Promise.reject(err),
          needAuth,
        );
      });
  },

  /**
   * Send request with Content-Type multipart/form
   * used to upload file
   * @param {Sring} url '/path/to/endpoint'
   * @param {Object} data
   */
  postData: (baseURL, url, data = {}, customConfig = {}) => {
    api.defaults.headers["Content-Type"] = "multipart/form-data";
    api.defaults.timeout = TIMEOUT;
    const formData = new FormData();
    const keys = Object.keys(data);
    keys.map((key) => {
      data[key] instanceof File
        ? formData.append(key, data[key], data[key].name)
        : formData.append(key, data[key]);
    });
    return api
      .post(url, formData, { baseURL: baseURL || BASE_URL, ...customConfig })
      .then((response) => {
        return APIResponseValidation(
          response,
          Promise.resolve(response),
          needAuth,
        );
      })
      .catch((err) => {
        return APIResponseValidation(
          err.response,
          Promise.reject(err),
          needAuth,
        );
      });
  },

  /**
   * @param {Sring} url '/path/to/endpoint'
   * @param {Object} params
   * {
   *   id: [1,2,3]
   * }
   */
  delete: (url, params, needAuth = true) => {
    let newUrl = url;
    if (params) {
      const qparam = querystring.stringify(params);
      newUrl = `${newUrl}?${qparam}`;
    }
    return api
      .delete(newUrl)
      .then((response) => {
        return APIResponseValidation(
          response,
          Promise.resolve(response),
          needAuth,
        );
      })
      .catch((err) => {
        return APIResponseValidation(
          err.response,
          Promise.reject(err),
          needAuth,
        );
      });
  },
  /**
   * @param {Sring} url '/path/to/endpoint'
   * @param {Object} payload
   * {
   *   id: [1,2,3]
   * }
   */
  deleteData: (url, payload, needAuth = true) => {
    return api
      .delete(url, { data: payload }, ((needAuth = true), (toastError = true)))
      .then((response) => {
        return APIResponseValidation(
          response,
          Promise.resolve(response),
          needAuth,
        );
      })
      .catch((err) => {
        return APIResponseValidation(
          err.response,
          Promise.reject(err),
          needAuth,
        );
      });
  },
  /**
   * Send request with Content-Type multipart/form
   * used to upload file
   * @param {Sring} url '/path/to/endpoint'
   * @param {Object} data
   */
  putData: (url, data = {}, customConfig = {}, needAuth = true) => {
    api.defaults.headers["Content-Type"] = "multipart/form-data";
    api.defaults.timeout = TIMEOUT;
    const formData = new FormData();
    const keys = Object.keys(data);
    keys.map((key) => {
      data[key] instanceof File
        ? formData.append(key, data[key], data[key].name)
        : formData.append(key, data[key]);
    });
    return api
      .put(url, formData, { ...customConfig })
      .then((response) => {
        return APIResponseValidation(
          response,
          Promise.resolve(response),
          needAuth,
        );
      })
      .catch((err) => {
        return APIResponseValidation(
          err.response,
          Promise.reject(err),
          needAuth,
        );
      });
  },
};

export default APIInstance;
