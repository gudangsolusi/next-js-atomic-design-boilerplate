const Button = ({ onClick }) => {
  return (
    <button
      onClick={onClick}
      className="h-[42px] rounded-lg bg-blue-500 px-6 text-white shadow-lg"
    >
      Cari
    </button>
  );
};

export default Button;
