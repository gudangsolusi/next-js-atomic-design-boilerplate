const AppLogo = () => {
  return <button className="rounded-lg text-5xl">Simple PokeDex</button>;
};

export default AppLogo;
