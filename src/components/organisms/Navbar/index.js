import AppLogo from "@/components/atoms/AppLogo";
import SearchBar from "@/components/molecules/SearchBar";
import constants from "@/constants";

const Navbar = () => {
  return (
    <div
      style={{
        height: constants.NAVBAR_HEIGHT,
      }}
      className="flex flex-1 flex-row items-center justify-between bg-white px-12 text-black"
    >
      <AppLogo />
      <SearchBar />
    </div>
  );
};

export default Navbar;
