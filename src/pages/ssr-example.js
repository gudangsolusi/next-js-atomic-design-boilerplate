import { useSelector } from "react-redux";

import Layout from "@/components/templates/Layout";
import { getAllPokemon } from "@/services/pokemon";

const Home = ({ pokemon }) => {
  const { searchValue } = useSelector((state) => state.example);

  return (
    <main className={`flex min-h-screen flex-col`}>
      <Layout>
        <div className="flex flex-1 flex-col text-center">
          <div className="flex min-h-[200px] items-center justify-center text-8xl">
            {searchValue ? searchValue : "Pokemons"}
          </div>
          <div className="flex flex-col">
            {pokemon.map((poke) => {
              return <div key={poke.name}>{poke.name}</div>;
            })}
          </div>
        </div>
      </Layout>
    </main>
  );
};

export default Home;

export async function getServerSideProps() {
  // This request will be happen on ServerSide
  const pokemon = (await getAllPokemon()) || [];

  return {
    props: {
      // The API Response will be send simultaneously with the page HTML
      pokemon: pokemon?.data?.results,
    },
  };
}
