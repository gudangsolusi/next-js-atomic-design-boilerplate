import { useSelector } from "react-redux";

import Layout from "@/components/templates/Layout";

const Home = () => {
  const { searchValue } = useSelector((state) => state.example);

  return (
    <main className={`flex min-h-screen flex-col`}>
      <Layout>
        <div className="flex h-full flex-1 items-center justify-center text-center">
          <div className="my-auto text-8xl">
            {searchValue ? searchValue : "The Boilerplate"}
          </div>
        </div>
      </Layout>
    </main>
  );
};

export default Home;
