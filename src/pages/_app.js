import Head from "next/head";
import { Provider } from "react-redux";

import { store } from "@/store";
import "@/styles/globals.css";

const App = ({ Component, pageProps }) => (
  <>
    <Head>
      <title>Next.Js: Atomic design boilerplate</title>
    </Head>
    <Provider store={store}>
      <Component {...pageProps} />
    </Provider>
  </>
);

export default App;
