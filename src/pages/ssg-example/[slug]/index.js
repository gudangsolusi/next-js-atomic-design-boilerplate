import Image from "next/image";

import Layout from "@/components/templates/Layout";
import { getAllPokemon, getPokemonByName } from "@/services/pokemon";

const Home = ({ pokemonData }) => {
  return (
    <main className={`flex min-h-screen flex-col`}>
      <Layout>
        <Image
          alt="pokemon"
          width={300}
          height={300}
          src={pokemonData?.sprites?.other["official-artwork"]?.front_default}
        />
        <div>Pokemon name : {pokemonData?.name}</div>
        <div>
          Pokemon type :{" "}
          {pokemonData?.types?.map((type) => type?.type?.name + " ")}
        </div>
      </Layout>
    </main>
  );
};

export default Home;

export async function getStaticPaths() {
  try {
    const res = await getAllPokemon();
    // This will generate path for 25 first pokemon from the PokeAPI, rest of pokemons will be generated on the fly
    // since we implement fallback: 'blocking', see the return value section
    const paths = res?.data?.results?.map((data) => ({
      params: { slug: data.name },
    }));

    return {
      paths,
      // There is 3 possible value for fallback. Please read below wiki article ,on Fallback section
      // https://wiki.aigs-rnd.tech/front-end/next-js/rendering-method/static-site-generation
      fallback: "blocking", // new paths not returned by getStaticPaths will wait for the HTML to be generated, identical to SSR (hence why blocking), and then be cached for future requests so it only happens once per path.
    };
  } catch (err) {
    console.error(err);

    return {
      paths: [],
      fallback: false,
    };
  }
}

export async function getStaticProps(ctx) {
  const res = await getPokemonByName(ctx.params.slug);

  if (res) {
    return {
      props: {
        pokemonData: res.data,
      },
    };
  }

  return {
    props: {
      pokemonData: [],
    },
  };
}
