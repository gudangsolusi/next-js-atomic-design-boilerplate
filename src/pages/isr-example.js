import Image from "next/image";

import Layout from "@/components/templates/Layout";
import { getAllPokemon, getPokemonByName } from "@/services/pokemon";

const Home = ({ pokemonData }) => {
  return (
    <main className={`flex min-h-screen flex-col`}>
      <Layout>
        <Image
          alt="pokemon"
          width={300}
          height={300}
          src={pokemonData?.sprites?.other["official-artwork"]?.front_default}
        />
        <div>Pokemon name : {pokemonData?.name}</div>
        <div>
          Pokemon type :{" "}
          {pokemonData?.types?.map((type) => type?.type?.name + " ")}
        </div>
      </Layout>
    </main>
  );
};

export async function getStaticPaths() {
  try {
    const res = await getAllPokemon();
    const paths = res?.data?.results?.map((data) => ({
      params: { slug: data.name },
    }));

    return {
      paths,
      fallback: false,
      re,
    };
  } catch (err) {
    console.error(err);

    return {
      paths: [],
      fallback: false,
    };
  }
}

export async function getStaticProps(ctx) {
  const res = await getPokemonByName(ctx.params.slug);

  if (res) {
    return {
      props: {
        pokemonData: res.data,
      },
      // The first request after the 60-second interval will trigger a revalidation,
      // regenerating the page in the background and updating the cache.
      // Subsequent requests within the next 60 seconds will serve the cached version,
      // and this cycle continues.
      revalidate: 60,
    };
  }

  return {
    props: {
      pokemonData: [],
    },
  };
}

export default Home;
