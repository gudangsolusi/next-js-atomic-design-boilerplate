/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,

  images: {
    // use this to allow only some domains to load images
    domains: ["raw.githubusercontent.com"],
  },

  // use below configuration to allow all image domains to load images
  // remotePatterns: [
  //   {
  //     protocol: "https",
  //     hostname: "**",
  //   },
  // ],
};

const withBundleAnalyzer = require("@next/bundle-analyzer")({
  enabled: process.env.ANALYZE === "true",
});

module.exports = withBundleAnalyzer(nextConfig);
